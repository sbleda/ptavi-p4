#!/usr/bin/python3
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import json
import time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    diccionario = {}
    dic = {}

    def register2json(self):
        """Creo un json."""
        with open('registered.json', 'w') as f:
                json.dump(self.diccionario, f, indent=2)

    def json2registered(self):
        """Obtengo de un json."""
        with open('registered.json', 'r') as fi:
                data = json.load(fi)
                self.diccionario = data

    def handle(self):
        """handle method of the server class."""
        """(all requests will be handled by this method)."""

        self.wfile.write(b"Hemos recibido tu peticion")
        SIPRegisterHandler.json2registered(self)

        for line in self.rfile:

            print("El cliente nos manda ", line.decode('utf-8'))
            var = line.decode('utf-8')
            vari = var.split(" ")

            if vari[0] == 'REGISTER':
                variable = vari[1].split(":")
                nombre = variable[1]
                self.dic['address:'] = self.client_address[0]
            self.dic = {'address:': self.client_address[0], 'expire:': ''}
            if vari[0] == 'Expires:':
                if (vari[1]) == '0\r\n':
                    try:
                        del self.diccionario[nombre]
                    except KeyError:
                        self.wfile.write(b"\nSIP/2.0 404 NOT FOUND\r\n\r\n")
                else:
                    tiempo = int(vari[1])
                    timepo = (time.strftime('%Y-%m-%d %H:%M:%S+ ' +
                                            str(tiempo), time.localtime()))
                    self.dic['expire:'] = timepo
                    self.diccionario[nombre] = self.dic
                    self.wfile.write(b"\nSIP/2.0 200 OK\r\n\r\n")
        print(self.diccionario)
        SIPRegisterHandler.register2json(self)


if __name__ == "__main__":
    try:
        PORT = int(sys.argv[1])
    except IndexError:
        sys.exit("Usage: PORT")

    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
