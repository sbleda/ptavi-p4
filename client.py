#!/usr/bin/python3
"""Programa cliente UDP que abre un socket a un servidor."""

import socket
import sys

print("Socket terminado.")
# Constantes. Dirección IP del servidor y contenido a enviar
# SERVER = 'localhost'
# PORT = 6001
# LINE = '¡Hola mundo!'
if __name__ == "__main__":
    try:
        PORT = int(sys.argv[2])
        SERVER = (sys.argv[1])
        REGISTER = str(sys.argv[3])
        DIRECCION = str(sys.argv[4])
        TIEMPO = (sys.argv[5])
    except IndexError:
        sys.exit("Usage: client.py ip puerto "
                 "register sip_address expires_value")

    if REGISTER == 'register':
        linea = ("REGISTER sip:" + DIRECCION +
                 " SIP/2.0\r\n" + "Expires: " + TIEMPO + '\r\n\r\n')

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.connect((SERVER, PORT))
        print("Enviando:", linea)
        my_socket.send(bytes(linea, 'utf-8') + b'\r\n')
        data = my_socket.recv(1024)
        print('Recibido -- ', data.decode('utf-8'))

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
